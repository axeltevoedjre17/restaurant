<?php
  $titre = "Acceder votre compte";
?>
    <link rel="stylesheet" href="css/style.css" />
    <link
      href="https://fonts.googleapis.com/css2?family=Roboto:wght@300&display=swap"
      rel="stylesheet"
    />
  </head>

   <br><br> <br><br>
  <body style="background-image: url(img/rgbg.jpg)">

    <div class="login-box">
      <h1 class="rg">Acceder votre compte</h1>
      <form action="sql_signin.php" method="post">

        <label>Email</label>
        <input type="email" class="form-control" id="mail" required="required" name="le_email">

        <label>Password</label>
        <input type="password" class="form-control" id="pass" required="required" name="le_pass">

        <button class="rgb" type="submit">Se Connecter</button>


      <closeform></closeform></form>
    </div><br>
    <p class="para-2">
      Not have an account? <a href="register.php">Sign Up Here</a> | <a href="index.php">Home</a> <br> If you want to login admin account <a href="admin-login.php">Admin-Login</a>
    </p>
  </body>

