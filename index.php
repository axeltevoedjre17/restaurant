<?php
  $titre = "Page d'accueil";
// Affichage des messages à l'aide de Boostrap 
// (composant alert)
  if(isset($_SESSION['message'])) {
    echo '<div class="alert alert-primary" role="alert">';
    echo $_SESSION['message'];
    echo '</div>';
    unset($_SESSION['message']);
  }
?>

<head>
     <title></title>
         <link rel="stylesheet" type="text/css" href="css/style.css">
         <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
<body>


  <header>

<!--header video-->
   <div class="overlay"></div>
      <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
      <source src="img/head.mp4" type="video/mp4">
      </video>

<!----nav bar ---->
     <?php
$titre = "Menu";
include 'menu.inc.php';

?>

<!-- Title -->
    <div class="section1">
        <h1 class="hs">Bienvenue à Esig Restaurant</h1><br>
        <button class="btnn btn1" onclick=window.location.href="cart/price.php" type="submit">MENU</button>
            </div>
        </div>
    </div>
</header>



<!-- Footer -->
 
<?php 
  include 'footer.inc.php';
?> 

<script>
      $(".slider").owlCarousel({
        loop: true,
        autoplay: true,
        autoplayTimeout: 2000, // time eka slide vena speed eka 2000ms = 2s;
        autoplayHoverPause: true,
      });
    </script>

</body>