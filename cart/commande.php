<?php

@include 'param.php';


?>

<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Commandes</title>

   <!-- font awesome cdn link  -->
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">

   <!-- custom css file link  -->
   <link rel="stylesheet" href="css/style.css">

</head>
<body>

<?php include 'headerCom.php'; ?>

<div class="container">

<section class="shopping-cart">

   <h1 class="heading">Commandes</h1>

   <table>

      <thead>
       <tr>
         <th>Order id</th>
         <th>Nom</th>
         <th>Produits</th>
         <th>Prix Total</th>
         <th>Status</th>
        </tr>
      </thead>
      <?php 
           $servername="localhost";
           $username = "root";
           $password = "root";
           $dbname = "bdd_4_14";
           $conn = mysqli_connect($servername, $username, $password, $dbname);
           if (!$conn)     
    {
      die("Connection failed: " . mysqli_connect_error());
     }
           $query = "SELECT * FROM `order`";
           $data = mysqli_query($conn,$query);
           while($rows = mysqli_fetch_array($data)){
            ?>
           
         
      <tbody>
      <tr>
         <td><?php echo $rows['id'];?></td>
         <td><?php echo $rows['name'];?></td>
         <td><?php echo $rows['total_products'];?></td>
         <td><?php echo $rows['total_price'];?></td>
         
         <td><?php if($rows['status'] == 1){
            echo "prete";
             
         }
         else{
            echo '<p><a href ="commande_status.php?
            id='.$rows['id'].'
             &status=1">Preter</a></p>';
             
         }
         ?></td>
         <?php }?>
        </tr>   
      </tbody>
   </table>


</section>

</div>
   
<!-- custom js file link  -->
<script src="js/script.js"></script>

</body>
</html>