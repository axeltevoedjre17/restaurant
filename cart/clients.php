<?php

@include 'config.php';


?>

<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Clients</title>

   <!-- font awesome cdn link  -->
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">

   <!-- custom css file link  -->
   <link rel="stylesheet" href="css/style.css">

</head>
<body>

<?php include 'header.php'; ?>

<div class="container">

<section class="shopping-cart">

   <h1 class="heading">Clients</h1>

   <table>

      <thead>
       <tr>
         <th>username</th>
         <th>firstname</th>
         <th>lastname</th>
         <th>password</th>
         <th>email</th>
         <th>status</th>
        </tr>
      </thead>
      <?php 
           $servername="localhost";
           $username = "root";
           $password = "root";
           $dbname = "bdd_4_14";
           $conn = mysqli_connect($servername, $username, $password, $dbname);
           if (!$conn)     
    {
      die("Connection failed: " . mysqli_connect_error());
     }
           $query = "SELECT * FROM `user_details`";
           $data = mysqli_query($conn,$query);
           while($rows = mysqli_fetch_array($data)){
            ?>
           
         
      <tbody>
      <tr>
         <td><?php echo $rows['username'];?></td>
         <td><?php echo $rows['firstname'];?></td>
         <td><?php echo $rows['lastname'];?></td>
         <td><?php echo $rows['password'];?></td>
         <td><?php echo $rows['email'];?></td>
         <td><?php if($rows['status'] == 1){
            echo '<p><a href ="status.php?
            username='.$rows['username'].'
             &status=0">disable</a></p>'; 
             
         }
         else{
            echo '<p><a href ="status.php?
            username='.$rows['username'].'
             &status=1">enable</a></p>';
             
         }
         ?></td>
         <?php }?>
        </tr>   
      </tbody>
   </table>


</section>

</div>
   
<!-- custom js file link  -->
<script src="js/script.js"></script>

</body>
</html>