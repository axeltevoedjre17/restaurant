<?php
  $titre = "Acceder votre compte";
  include 'header.inc.php';
  include 'menu.inc.php';
?>
<link rel="stylesheet" href="StyleMenu.css">
    <h4>Acceder votre compte</h4>
    <div class="container">
      <form class="row g-3" action="sql_seconnecter.php" method="post"> 
        <div class="col-md-6">
          <label for="nom" class="form-label">Identifiant</label>
          <input type="text" class="form-control" id="nom" required name="le_nom">
        </div>
        <div class="col-md-6">
          <label for="pass" class="form-label">Password</label>
          <input type="password" class="form-control" id="pass" required name="le_pass">
        </div>
        <div class="row my-3">
      <div class="d-grid gap-2 d-md-block"><button class="btn btn-outline-primary" type="submit">Valider</button></div>  
    </div>
        
      </form>
       <div class="d-grid gap-2 d-md-block">
        <button class="btn btn-outline-primary" onclick=window.location.href="creationCompte.php" type="submit">Creer compte</button>
    </div>
    
<?php 
  include 'footer.inc.php';
?> 